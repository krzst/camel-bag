package com.mockedcamel.bag.cloud.model;

import com.mockedcamel.bag.cloud.model.validate.ErrorMsg;
import org.springframework.util.Assert;

public class StepResult {

    private String name;
    private boolean status = false;

    public StepResult(final String name) {
        Assert.notNull(name, ErrorMsg.OBJECT_NULL);

        this.name = name;
    }

    public void valid() {
        status = true;
    }

    public void invalid() {
        status = false;
    }

    public boolean isStatusOk() {
        return status;
    }
}
