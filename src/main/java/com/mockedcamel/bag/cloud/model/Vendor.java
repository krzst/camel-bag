package com.mockedcamel.bag.cloud.model;

public enum Vendor {
    google_drive, icloud, dropbox, aws_s3
}