package com.mockedcamel.bag.cloud.model;

import com.mockedcamel.bag.cloud.model.validate.ErrorMsg;
import org.springframework.util.Assert;

public class Account {

    private String username;

    private String vendor;

    private String bucket;

    public Account(final String username, final String vendor, final String bucket) {
        Assert.notNull(username, ErrorMsg.OBJECT_NULL);
        Assert.notNull(vendor, ErrorMsg.OBJECT_NULL);
        Assert.notNull(bucket, ErrorMsg.OBJECT_NULL);

        this.username = username;
        this.vendor = vendor;
        this.bucket = bucket;
    }

    public String getUsername() {
        return username;
    }

    public String getVendor() {
        return vendor;
    }

    public String getBucket() {
        return bucket;
    }
}
