package com.mockedcamel.bag.cloud.provider;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LimitedS3CredentialsProvider implements AWSCredentialsProvider {

    private String id;
    private String key;

    public LimitedS3CredentialsProvider(@Value("${aws.access.s3_limited.id}") String id, @Value("${aws.access.s3_limited.key}") String key) {
        System.out.println("S3 LIMITED ###############");

        System.out.println(id);
        System.out.println(key);

        this.id = id.trim();
        this.key = key.trim();
    }

    @Override
    public void refresh() {}

    @Override
    public AWSCredentials getCredentials() {
        return new AWSCredentials() {

            @Override
            public String getAWSSecretKey() {
                return key;
            }

            @Override
            public String getAWSAccessKeyId() {
                return id;
            }
        };
    }
}
