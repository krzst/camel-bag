package com.mockedcamel.bag.cloud.provider;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AdminCredentialsProvider implements AWSCredentialsProvider {

    private String id;
    private String key;

    public AdminCredentialsProvider(@Value("${aws.access.admin.id}") String id, @Value("${aws.access.admin.key}") String key) {
        System.out.println("ADMIN ###############");

        System.out.println(id);
        System.out.println(key);

        this.id = id.trim();
        this.key = key.trim();
    }

    @Override
    public void refresh() {}

    @Override
    public AWSCredentials getCredentials() {
        return new AWSCredentials() {

            @Override
            public String getAWSSecretKey() {
                return key;
            }

            @Override
            public String getAWSAccessKeyId() {
                return id;
            }
        };
    }
}
