package com.mockedcamel.bag.cloud.service;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.mockedcamel.bag.cloud.model.Account;
import com.mockedcamel.bag.cloud.model.StepResult;
import com.mockedcamel.bag.cloud.model.validate.ErrorMsg;
import com.mockedcamel.bag.cloud.provider.LimitedDynamodbCredentialsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

@Service
public class AccountService {

    public static final String TABLE_NAME = "so_users";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_VENDOR = "vendor";
    public static final String COLUMN_BUCKET = "bucket";

    @Autowired
    private LimitedDynamodbCredentialsProvider limitedDynamodbCredentialsProvider;

    public StepResult create(final Account account, final Regions region) {
        Assert.notNull(account, ErrorMsg.OBJECT_NULL);
        Assert.notNull(region, ErrorMsg.OBJECT_NULL);

        final StepResult result = new StepResult(this.getClass().getCanonicalName());

        AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClient.builder().withRegion(region).withCredentials(limitedDynamodbCredentialsProvider).build();

        PutItemRequest putItemRequest = new PutItemRequest();
        putItemRequest.withTableName(TABLE_NAME);

        Map<String, AttributeValue> item = new HashMap<>();
        item.put(COLUMN_USERNAME, new AttributeValue().withS(account.getUsername()));
        item.put(COLUMN_VENDOR, new AttributeValue().withS(account.getVendor()));
        item.put(COLUMN_BUCKET, new AttributeValue().withS(account.getBucket()));

        putItemRequest.withItem(item);
        amazonDynamoDB.putItem(putItemRequest);

        GetItemRequest getItemRequest = new GetItemRequest();
        getItemRequest.withTableName(TABLE_NAME);
        getItemRequest.addKeyEntry(COLUMN_USERNAME, new AttributeValue().withS(account.getUsername()));
        getItemRequest.addKeyEntry(COLUMN_VENDOR, new AttributeValue().withS(account.getVendor()));

        GetItemResult getItemResult = amazonDynamoDB.getItem(getItemRequest);
        if (getItemResult.getItem() != null) {
            result.valid();
        }

        return result;
    }
}
