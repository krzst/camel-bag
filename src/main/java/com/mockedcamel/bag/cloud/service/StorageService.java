package com.mockedcamel.bag.cloud.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.mockedcamel.bag.cloud.model.Account;
import com.mockedcamel.bag.cloud.model.StepResult;
import com.mockedcamel.bag.cloud.model.validate.ErrorMsg;
import com.mockedcamel.bag.cloud.provider.LimitedS3CredentialsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class StorageService {

    @Autowired
    private LimitedS3CredentialsProvider limitedS3CredentialsProvider;

    public StepResult create(final Account account) {
        Assert.notNull(account, ErrorMsg.OBJECT_NULL);

        final StepResult result = new StepResult(this.getClass().getCanonicalName());


        final AmazonS3 s3Client = AmazonS3Client.builder().withCredentials(limitedS3CredentialsProvider).build();

        if (!s3Client.doesBucketExistV2(account.getBucket())) {
            CreateBucketRequest createBucketRequest = new CreateBucketRequest(account.getBucket());

            s3Client.createBucket(createBucketRequest);

            if (s3Client.doesBucketExistV2(account.getBucket())) {
                result.valid();
            }
        }

        return result;
    }
}
