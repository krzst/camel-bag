package com.mockedcamel.bag.cloud.controller;

import com.amazonaws.regions.Regions;
import com.mockedcamel.bag.cloud.model.Account;
import com.mockedcamel.bag.cloud.model.StepResult;
import com.mockedcamel.bag.cloud.model.Vendor;
import com.mockedcamel.bag.cloud.service.AccountService;
import com.mockedcamel.bag.cloud.service.StorageService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IAMController {

    public static final String BUCKET_PREFIX = "bucket-mocked-camel-";

    @Autowired
    private AccountService accountService;

    @Autowired
    private StorageService storageService;

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void createAccount(final String username) {
        final String bucketName = IAMController.BUCKET_PREFIX + DigestUtils.sha1Hex(username + System.currentTimeMillis());
        final Account account = new Account(username, Vendor.google_drive.name(), bucketName);

        StepResult stepResult = accountService.create(account, Regions.EU_WEST_1);
        if (stepResult.isStatusOk()) {
            storageService.create(account);
        }
    }
}
