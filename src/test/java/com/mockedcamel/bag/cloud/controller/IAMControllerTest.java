package com.mockedcamel.bag.cloud.controller;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ListBucketsRequest;
import com.mockedcamel.bag.cloud.provider.AdminCredentialsProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IAMControllerTest {

	@Autowired
	private IAMController iamController;

	@Autowired
	private AdminCredentialsProvider adminCredentialsProvider;

	@Before
	public void setUp() {
		AmazonS3 s3ClientAdmin = AmazonS3Client.builder().withCredentials(adminCredentialsProvider).build();

		ListBucketsRequest listBucketRequest = new ListBucketsRequest();
		List<Bucket> buckets = s3ClientAdmin.listBuckets(listBucketRequest);

		buckets.stream().filter(bucket -> bucket.getName().startsWith(IAMController.BUCKET_PREFIX)).forEach(bucket -> {
			s3ClientAdmin.deleteBucket(bucket.getName());
		});
	}

	@Test
	public void contextLoads() {
		iamController.createAccount("krzst@o2.pl");
	}

}
